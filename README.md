# IFCExtract

IFCExtract is a small tool for extracting parts of an IFC file to a new file, typically for debugging. Note
that the resulting IFC files isn't necessarily valid because key elements such as `IFCSITE` might be missing from
the result, but it is still useful e.g. for testing with [IfcOpenShell](https://github.com/IfcOpenShell/IfcOpenShell).

## Usage

`./ifcextract -input [ifc] -output [ifc] -line [line1] -line [line2] ... -line [lineN]`

where

- `ifc` - Input and output IFC files
- `line` is a line identifier to extract

The application will create a new IFC file that contains all references of the extracted lines (recursive). This
is useful to debug IFC files by extracting problematic entries.

Make sure to include the line of IFCPROJECT in order to generate a file that contains correct unit assignments.

## Installation

Run 

`go get bitbucket.org/rendradev/ifcextract`
`go build`


## License

Licensed under GPLv3. Copyright 2017 Rendra.

See LICENSE.