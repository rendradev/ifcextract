package main

import (
	"fmt"
	"regexp"
	"strconv"
)

type line struct {
	raw          string
	ifcID        string
	lineID       int
	referenceIDs []int
}

var stepPrefixRegex = regexp.MustCompile(`^#\d+=`)
var ifcIDRegex = regexp.MustCompile(`^#\d+=\w+\(.*'(\w+)'.*\);$`)
var lineIDRegex = regexp.MustCompile(`^#(\d+)=`)
var referenceIDsRegex = regexp.MustCompile(`#(\d+)[^\d=]`)

func isHeaderLine(s string) bool {
	return !stepPrefixRegex.MatchString(s)
}

func parseLine(s string) (*line, error) {
	lineIDMatch := lineIDRegex.FindStringSubmatch(s)
	if lineIDMatch == nil {
		return nil, fmt.Errorf("Line '%s' is not a valid STEP entry", s)
	}
	ifcIDMatch := ifcIDRegex.FindStringSubmatch(s)
	referenceIDsMatch := referenceIDsRegex.FindAllStringSubmatch(s, -1)

	lineID, err := strconv.Atoi(lineIDMatch[1])
	if err != nil {
		return nil, fmt.Errorf("Line '%s' has invalid line ID: %v", s, err)
	}
	var ifcID string
	if ifcIDMatch != nil {
		ifcID = ifcIDMatch[1]
	}
	var referenceIDs []int
	referenceIDs = make([]int, len(referenceIDsMatch))
	for i := 0; i < len(referenceIDs); i++ {
		referenceIDs[i], err = strconv.Atoi(referenceIDsMatch[i][1])
		if err != nil {
			return nil, fmt.Errorf("Encountered invalid reference ID '%s' for line '%s': %v", referenceIDsMatch[i][0], s, err)
		}
	}
	return &line{
		raw:          s,
		ifcID:        ifcID,
		lineID:       lineID,
		referenceIDs: referenceIDs,
	}, nil
}
