package main

import (
	"bufio"
	"flag"
	"fmt"
	"log"
	"os"
	"sort"
	"strconv"
	"strings"
)

type lineFlags []int

func (l *lineFlags) String() string {

	return strings.Trim(strings.Join(strings.Fields(fmt.Sprint(*l)), ","), "[]")
}

func (l *lineFlags) Set(value string) error {
	asInt, err := strconv.Atoi(value)
	if err != nil {
		return err
	}
	*l = append(*l, asInt)
	return nil
}

var inputFile string
var outputFile string
var extractLineIDs lineFlags

func parseArguments() {
	flag.StringVar(&inputFile, "input", inputFile, "Input IFC file")
	flag.StringVar(&outputFile, "output", inputFile, "Output IFC file")
	flag.Var(&extractLineIDs, "line", "LineID (#1234=...) to extract. Multiple entries allowed")
	flag.Parse()
}

func processChunk(entities map[int]*line, writer *bufio.Writer) error {
	toExtract := make(map[int]bool)
	for _, extractLineID := range extractLineIDs {
		err := determineLinesToExtract(extractLineID, entities, toExtract)
		if err != nil {
			return err
		}
	}
	sortedIDs := make([]int, 0, len(toExtract))
	for id := range toExtract {
		sortedIDs = append(sortedIDs, id)
	}
	sort.Sort(sort.IntSlice(sortedIDs))

	for _, lineID := range sortedIDs {
		writer.WriteString(entities[lineID].raw)
		writer.WriteRune('\n')
	}
	return nil
}

func main() {
	parseArguments()

	file, err := os.Open(inputFile)
	if err != nil {
		log.Fatalf("Could not open file '%s': %v", inputFile, err)
	}
	defer file.Close()

	output, err := os.OpenFile(outputFile, os.O_CREATE|os.O_RDWR|os.O_TRUNC, 0644)
	if err != nil {
		panic(fmt.Errorf("Could not open '%s' for writing: %v", outputFile, err))
	}
	defer output.Close()
	writer := bufio.NewWriter(output)
	defer writer.Flush()

	fmt.Printf("Loading file '%s'", inputFile)
	entities := make(map[int]*line)
	scanner := bufio.NewScanner(file)
	scanner.Buffer(make([]byte, 1024*1024), 10*1024*1024) // Accept really long lines
	for scanner.Scan() {
		var str = scanner.Text()
		if isHeaderLine(str) {
			if len(entities) > 0 {
				fmt.Printf("\nProcessing %d lines\n", len(entities))
				// Process chunk between header and footer
				if err = processChunk(entities, writer); err != nil {
					panic(err)
				}
				entities = make(map[int]*line)
			}
			// Flush header directly to writer
			writer.WriteString(str)
			writer.WriteRune('\n')
		} else {
			// Multiline statement?
			for !strings.HasSuffix(str, ";") && scanner.Scan() {
				str = str + scanner.Text()
			}
			line, err := parseLine(str)
			if err != nil {
				panic(err)
			}
			entities[line.lineID] = line
			if line.lineID%100000 == 0 {
				fmt.Printf(".")
			}
		}
	}
	if scanner.Err() != nil {
		panic(scanner.Err())
	}
	if len(entities) > 0 {
		panic(fmt.Errorf("Expected file to end with footer, but got entries"))
	}
	fmt.Println("Done!")
}

func determineLinesToExtract(id int, allLines map[int]*line, markedIDs map[int]bool) error {
	markedIDs[id] = true
	if _, found := allLines[id]; !found {
		return fmt.Errorf("No line with id %d found in file", id)
	}
	for _, refID := range allLines[id].referenceIDs {
		if _, found := markedIDs[refID]; !found {
			if err := determineLinesToExtract(refID, allLines, markedIDs); err != nil {
				return err
			}
		}
	}
	return nil
}
